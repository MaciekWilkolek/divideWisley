package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.other.DeptorOrCreditor;
import com.example.divideWisely.repository.CounterpartRepository;
import com.example.divideWisely.repository.UserDataRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CounterpartServiceTest {
    @Mock
    CounterpartRepository counterpartRepository;
    @Mock
    UserDataRepository userDataRepository;
    @InjectMocks
    CounterpartService counterpartService;

    @Test
    void shouldUpDateCounterpartForOneSideCorrectly() throws DivideWisleyExeption {
        //given
        UserData userData1 = new UserData(null, "Maciek", "123", new HashSet<>());
        UserData userData2 = new UserData(null, "Kuba", "1234", new HashSet<>());
        Long userId = 1L;
        Long counterpartId = 2L;
        Mockito.when(userDataRepository.findById(userId)).thenReturn(Optional.of(userData1));
        Mockito.when(userDataRepository.findById(counterpartId)).thenReturn(Optional.of(userData2));
        //when
        String stringResponse = counterpartService.upDateCounterpartForOneSide(Collections.singleton(counterpartId), userId, 100.0, DeptorOrCreditor.DEPTOR);
        //then
        Assertions.assertEquals("poprawnie dodano Cunterparts", stringResponse);
    }

    @Test
    void shouldThrowExeptionWhenUserIsNotExist() throws DivideWisleyExeption {
        //given
        UserData userData1 = new UserData(null, "Maciek", "123", new HashSet<>());
        UserData userData2 = new UserData(null, "Kuba", "1234", new HashSet<>());
        Long userId = 1L;
        Long counterpartId = 2L;
        Mockito.when(userDataRepository.findById(userId)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class,()-> counterpartService.upDateCounterpartForOneSide(Collections.singleton(counterpartId), userId, 100.0, DeptorOrCreditor.DEPTOR));
    }
    @Test
    void shouldThrowExeptionWhenCounterpartsAreNotExist() throws DivideWisleyExeption {
        //given
        UserData userData1 = new UserData(null, "Maciek", "123", new HashSet<>());
        UserData userData2 = new UserData(null, "Kuba", "zlehaslo", new HashSet<>());
        Long userId = 1L;
        Long counterpartId = 2L;
        Mockito.when(userDataRepository.findById(userId)).thenReturn(Optional.of(userData1));
        Mockito.when(userDataRepository.findById(counterpartId)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class,()-> counterpartService.upDateCounterpartForOneSide(Collections.singleton(counterpartId), userId, 100.0, DeptorOrCreditor.DEPTOR));
    }


}
