package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UserLoginData;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class LoggedUserServiceTest {
    @Mock
    UserDataRepository userRepository;
    @Mock
    LoggedUserRepository loggedUserRepository;
    @InjectMocks
    LoggedUserService loggedUserService;

    @Test
    void shouldLoginUserCorrectly() throws DivideWisleyExeption {
        //given
        UserLoginData userLoginData = new UserLoginData("Maciek", "123");
        UserData userData = new UserData(1L, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.existsByName(userLoginData.getName())).thenReturn(true);
        Mockito.when(userRepository.findByName("Maciek")).thenReturn(Optional.of(userData));
        //when
        String token = loggedUserService.loginUser(userLoginData);
        //then
        //nie ma jak sprawdzic czy token jest poprawny bo jest zalerzny od czasu powstawania
    }

    @Test
    void shouldThrowExeptionWhenUserDoesNotExist() {
        //given
        UserLoginData userLoginData = new UserLoginData("Maciek", "123");
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> loggedUserService.loginUser(userLoginData));
    }

    @Test
    void shouldThrowExeptionWhenPasswordIsIncorrect() {
        //given
        UserLoginData userLoginData = new UserLoginData("Maciek", "zle haslo");
        Mockito.when(userRepository.existsByName(userLoginData.getName())).thenReturn(true);
        Mockito.when(userRepository.findByName("Maciek")).thenReturn(Optional.of(new UserData(1L, "Maciek", "123", new HashSet<>())));
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> loggedUserService.loginUser(userLoginData));
    }

    @Test
    void shouldLogoutUserCorrectly() throws DivideWisleyExeption {
        //given
        String sampleToken = "token123";
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        //when
        String message = loggedUserService.logoutUser(sampleToken);
        //then
        Assertions.assertEquals("poprawnie wylogowano", message);
    }

    @Test
    void shouldThrowWhenTokenDoesNotExist(){
        //given
        String sampleToken = "token123";
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(false);
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class,()->loggedUserService.logoutUser(sampleToken));
    }
}
