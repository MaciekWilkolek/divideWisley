package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.repository.FoundRepository;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import com.example.divideWisely.repository.UsersGroupRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UsersGroupServiceTest {
    @Mock
    FoundRepository foundRepository;
    @Mock
    UsersGroupRepository usersGroupRepository;
    @Mock
    CounterpartService counterpartService;
    @Mock
    LoggedUserRepository loggedUserRepository;
    @Mock
    UserDataRepository userDataRepository;
    @InjectMocks
    UsersGroupService usersGroupService;

    @Test
    void shouldCreateGroupCorrectly() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        UserData kuba = new UserData(2L, "Kuba", "123", new HashSet<>());
        String sampleToken = "token123";
        HashSet<UserData> users = new HashSet<>();
        users.add(maciek);
        users.add(kuba);
        UsersGroup usersGroup = new UsersGroup(users, "nazwa", new ArrayList<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(userDataRepository.findById(1l)).thenReturn(Optional.of(maciek));
        Mockito.when(userDataRepository.findById(2l)).thenReturn(Optional.of(kuba));
        //when
        String message = usersGroupService.createGroup(usersGroup, sampleToken);
        //then
        Assertions.assertEquals("grupa nazwa utworzona", message);
    }


    @Test
    void shouldThrowExeptionWhenUserIsNotLoggedIn() throws DivideWisleyExeption {
        //given
        String sampleToken = "token123";
        UsersGroup usersGroup = new UsersGroup(new HashSet<>(), "nazwa", new ArrayList<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(false);
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.createGroup(usersGroup, sampleToken));
    }

    @Test
    void shouldThrowExeptionWhenUsersAreNotExist() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        UserData kuba = new UserData(2L, "Kuba", "123", new HashSet<>());
        String sampleToken = "token123";
        HashSet<UserData> users = new HashSet<>();
        users.add(maciek);
        users.add(kuba);
        UsersGroup usersGroup = new UsersGroup(users, "nazwa", new ArrayList<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(userDataRepository.findById(1l)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.createGroup(usersGroup, sampleToken));
    }

    @Test
    void shouldThrowExeptionWhenGroupIsToSmall() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        HashSet<UserData> users = new HashSet<>();
        UsersGroup usersGroup = new UsersGroup(users, "nazwa", new ArrayList<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(userDataRepository.findById(1l)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.createGroup(usersGroup, sampleToken));
    }

    @Test
    void shouldGetGroupCorrectly() throws DivideWisleyExeption {
        //given
        long id = 1L;
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        HashSet<UserData> members = new HashSet<>();
        members.add(maciek);
        UsersGroup usersGroup = new UsersGroup(1L, members, "grupa1", new ArrayList<>());
        Mockito.when(usersGroupRepository.findById(id)).thenReturn(Optional.of(usersGroup));
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        //when
        UsersGroup userGroupFromMethode = usersGroupService.getGroup(id, sampleToken);
        //then
        Assertions.assertEquals(usersGroup, userGroupFromMethode);
    }

    @Test
    void shouldGetGroupCorrectlyWhenAdminAskForAGroup() throws DivideWisleyExeption {
        //given
        long id = 1L;
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String adminToken = "password";//moze bierz z jakis propertisow albo resources
        HashSet<UserData> members = new HashSet<>();
        members.add(maciek);
        UsersGroup usersGroup = new UsersGroup(1L, members, "grupa1", new ArrayList<>());
        Mockito.when(usersGroupRepository.findById(id)).thenReturn(Optional.of(usersGroup));
        //when
        UsersGroup userGroupFromMethode = usersGroupService.getGroup(id, adminToken);
        //then
        Assertions.assertEquals(usersGroup, userGroupFromMethode);
    }

    @Test
    void shouldThrowExeptionWhenUserIsNotExist() throws DivideWisleyExeption {
        //given
        long id = 1L;
        String sampleToken = "token123";
        Mockito.when(usersGroupRepository.findById(id)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.getGroup(id, sampleToken));
    }

    @Test
    void getGroupShouldThrowExeptionWhenUserIsNotLoggedIn() throws DivideWisleyExeption {
        //given
        long id = 1L;
        String sampleToken = "token123";
        UsersGroup usersGroup = new UsersGroup(1L, new HashSet<>(), "grupa1", new ArrayList<>());
        Mockito.when(usersGroupRepository.findById(id)).thenReturn(Optional.of(usersGroup));
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(false);
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.getGroup(id, sampleToken));
    }

    @Test
    void shouldFoundCorrectly() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        long groupId = 1L;
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(usersGroupRepository.findById(groupId)).thenReturn(Optional.of(new UsersGroup(1L, Collections.singleton(maciek), "grupa1", new ArrayList<>())));
        //when
        String message = usersGroupService.found("na pizze", 150.00, groupId, sampleToken);
        //then
        Assertions.assertEquals("poprawnie zafudowano na pizze", message);
    }

    @Test
    void shouldThrowWhenUserDoesNotBelongToGroup() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        UserData wojtek = new UserData(2L, "Wojtek", "123", new HashSet<>());
        String sampleToken = "token123";
        long groupId = 1L;
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(usersGroupRepository.findById(groupId)).thenReturn(Optional.of(new UsersGroup(1L, Collections.singleton(wojtek), "grupa1", new ArrayList<>())));
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.found("na pizze", 150.00, groupId, sampleToken));
    }
    @Test
    void shouldThrowWhenGroupIsNotFound() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        long groupId = 1L;
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(maciek, sampleToken)));
        Mockito.when(usersGroupRepository.findById(groupId)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.found("na pizze", 150.00, groupId, sampleToken));
    }
    @Test
    void shouldThrowExeptionWhenUserDoesNotLoggedIn() throws DivideWisleyExeption {
        //given
        UserData maciek = new UserData(1L, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        long groupId = 1L;
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> usersGroupService.found("na pizze", 150.00, groupId, sampleToken));
    }
}
