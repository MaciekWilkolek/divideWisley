package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import com.example.divideWisely.repository.UsersGroupRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class UserDataServiceTest {
    @Mock
    UserDataRepository userRepository;
    @Mock
    UsersGroupRepository usersGroupRepository;
    @Mock
    LoggedUserRepository loggedUserRepository;
    @InjectMocks
    UserDataService userDataService;

    @Test
    void shouldSaveUserCorrectly() throws DivideWisleyExeption {//throws DivideWisleyExeption -> controllerAdvice
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.existsByName(userData.getName())).thenReturn(false);
        UserData userDataToCompare = new UserData(1L, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.save(userData)).thenReturn(userDataToCompare);
        //when
        UserData userDataResponce = userDataService.addUserData(userData);
        //then
        assertEquals(userDataToCompare, userDataResponce);
    }

    @Test
    void shouldThrowExeptionWhenUserAlreadyExist() {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.existsByName(userData.getName())).thenReturn(true);
        //when & then
        assertThrows(DivideWisleyExeption.class, () -> userDataService.addUserData(userData));
    }

    @Test
    void shouldGetUserDataForAdminCorrectly() throws DivideWisleyExeption {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(userData));
        //when
        UserData userDataResponce = userDataService.getUserForAdmin(1L);
        //then
        Assertions.assertEquals(userData, userDataResponce);
    }

    @Test
    void shouldThrowExeptionWhenUserDataIsNotPresent() {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> userDataService.getUserForAdmin(1L));

    }

    @Test
    void shouldGetUserDataCorrectly() throws DivideWisleyExeption {
        //given
        String sampleToken = "token123";
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(userData, sampleToken)));
        //when
        UserData userDataResponce = userDataService.getUserData(sampleToken);
        //then
        Assertions.assertEquals(userData, userDataResponce);
    }

    @Test
    void shouldThrowExeptionWhenUserIsNotLogged() {
        //given
        String sampleToken = "token123";
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(false);
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> userDataService.getUserData(sampleToken));
    }


    @Test
    void shouldGetUserIdByNameCorrectly() throws DivideWisleyExeption {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        UserData userDataResponce = new UserData(1L, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.findByName(userData.getName())).thenReturn(Optional.of(userDataResponce));
        //when
        Long userIdByName = userDataService.getUserIdByName(userData.getName());
        //then
        Assertions.assertEquals(1L, userIdByName);
    }

    @Test
    void shouldThrowExeptionWhenUserIsNotExist() {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        Mockito.when(userRepository.findByName(userData.getName())).thenReturn(Optional.empty());
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> userDataService.getUserIdByName(userData.getName()));
    }

    @Test
    void shouldGetUserGroupsCorrectly() throws DivideWisleyExeption {
        //given
        UserData userData = new UserData(null, "Maciek", "123", new HashSet<>());
        String sampleToken = "token123";
        List<UsersGroup> listofGroups = new ArrayList<>();
        listofGroups.add(new UsersGroup(1L, new HashSet<>(), "group1", new ArrayList<>()));
        listofGroups.add(new UsersGroup(2L, new HashSet<>(), "group2", new ArrayList<>()));
        listofGroups.add(new UsersGroup(3L, new HashSet<>(), "group3", new ArrayList<>()));
        new HashSet<>(usersGroupRepository.findAllByMembersContaining(userData));
        Mockito.when(usersGroupRepository.findAllByMembersContaining(userData)).thenReturn(listofGroups);
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(true);
        Mockito.when(loggedUserRepository.findByToken(sampleToken)).thenReturn(Optional.of(new LoggedUser(userData, sampleToken)));
        //when
        Set<UsersGroup> userGroups = userDataService.getUserGroups(sampleToken);
        //then
        Assertions.assertEquals(new HashSet<>(listofGroups), userGroups);
    }


    @Test
    void shouldThrowExeptionWhenUserIsNotLoggedIn() throws DivideWisleyExeption {
        //given
        String sampleToken = "token123";
        Mockito.when(loggedUserRepository.existsByToken(sampleToken)).thenReturn(false);
        //when & then
        Assertions.assertThrows(DivideWisleyExeption.class, () -> userDataService.getUserGroups(sampleToken));
    }
}