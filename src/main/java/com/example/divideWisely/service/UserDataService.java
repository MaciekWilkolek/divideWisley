package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.Counterpart;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import com.example.divideWisely.repository.UsersGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserDataService {
    @Autowired
    UserDataRepository userRepository;
    @Autowired
    UsersGroupRepository usersGroupRepository;

    @Autowired
    LoggedUserRepository loggedUserRepository;

    public UserData addUserData(UserData userData) throws DivideWisleyExeption {
        if (userRepository.existsByName(userData.getName())) {
            throw new DivideWisleyExeption(HttpStatus.CONFLICT,"this user already exist");//walidacja pustych pol
        }
        return userRepository.save(userData);

    }

    public UserData getUserForAdmin(long userId) throws DivideWisleyExeption {
        Optional<UserData> userData = userRepository.findById(userId);
        if (userData.isPresent()) {
            userData.get().getCounterparts().forEach(counterpart -> counterpart.getUserData().setCounterparts(null));
            return userData.get();
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"this user does not exist");
        }
    }

    public UserData getUserData(String token) throws DivideWisleyExeption {
        if (loggedUserRepository.existsByToken(token)) {
            UserData userData = loggedUserRepository.findByToken(token).get().getUserData();
            userData.getCounterparts().forEach(counterpart -> deleteSensitiveDataForCounterPart(counterpart));
            return loggedUserRepository.findByToken(token).get().getUserData();
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"this user does not logged in");
        }
    }

    private Counterpart deleteSensitiveDataForCounterPart(Counterpart counterpart) {
        counterpart.getUserData().setPassword(null);
        counterpart.getUserData().setCounterparts(null);
        return counterpart;
    }

    public Set<UsersGroup> getUserGroups(String token) throws DivideWisleyExeption {
        if (loggedUserRepository.existsByToken(token)) {
            UserData userData = loggedUserRepository.findByToken(token).get().getUserData();
            Set<UsersGroup> userGroups = new HashSet<>(usersGroupRepository.findAllByMembersContaining(userData));
            userGroups.forEach(usersGroup -> usersGroup.getFounds().forEach(found -> found.getFounder().setCounterparts(null)));
            userGroups.forEach(usersGroup -> usersGroup.getMembers().forEach(userData1 -> userData1.setCounterparts(null)));
            return userGroups;
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"this user does not logged in");
        }

    }

    public Long getUserIdByName(String name) throws DivideWisleyExeption {
        if (userRepository.findByName(name).isPresent()) {
            return userRepository.findByName(name).get().getId();
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"this user does not exist");
        }
    }
}
