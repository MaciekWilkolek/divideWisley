package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.Counterpart;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.other.DeptorOrCreditor;
import com.example.divideWisely.repository.CounterpartRepository;
import com.example.divideWisely.repository.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class CounterpartService {
    @Autowired
    CounterpartRepository counterpartRepository;
    @Autowired
    UserDataRepository userDataRepository;

    public String upDateCounterpartForOneSide(Set<Long> counterpartIds, Long userId, Double prize, DeptorOrCreditor deptorOrCreditor) throws DivideWisleyExeption {

        Optional<UserData> userToChange = userDataRepository.findById(userId);
        if (userToChange.isPresent()) {
            Set<Counterpart> finalSet = new HashSet<>();

            Set<Counterpart> presentUserCounterparts = userToChange.get().getCounterparts();
            Set<Counterpart> newUserCounterparts = new HashSet<>();
            try {
                counterpartIds
                        .forEach(counterpartId -> newUserCounterparts.add(new Counterpart(userDataRepository.findById(counterpartId).get(), deptorOrCreditor, prize)));
            } catch (Exception e) {
                throw new DivideWisleyExeption(HttpStatus.BAD_REQUEST,"nie ma takich userów(Cunterparts)");
            }
            for (Counterpart newCounterpart : newUserCounterparts) {
                boolean presentCounterpartFound = false;
                for (Counterpart presentCounterpart : presentUserCounterparts) {
                    if (newCounterpart.getUserData().getId() == presentCounterpart.getUserData().getId()) {
                        if (newCounterpart.getDeptorOrCreditor().equals(presentCounterpart.getDeptorOrCreditor())) {
                            presentCounterpart.setPrize(newCounterpart.getPrize() + presentCounterpart.getPrize());
                        } else {
                            if ((newCounterpart.getPrize() - presentCounterpart.getPrize()) > 0) {
                                presentCounterpart.setDeptorOrCreditor(reverceDeptorOrCreditor(presentCounterpart.getDeptorOrCreditor()));
                                presentCounterpart.setPrize(newCounterpart.getPrize() - presentCounterpart.getPrize());
                            } else {
                                presentCounterpart.setPrize((newCounterpart.getPrize() - presentCounterpart.getPrize()) * -1);
                            }
                        }
                        finalSet.add(presentCounterpart);
                        presentCounterpartFound = true;
                    } else {
                        finalSet.add(presentCounterpart);
                    }
                }
                if (presentCounterpartFound == false) {
                    finalSet.add(newCounterpart);
                }

            }

            Set<Counterpart> finalSetWithoutZero = new HashSet<>(finalSet.stream().filter(counterpart -> counterpart.getPrize() != 0).toList());
            counterpartRepository.saveAll(finalSetWithoutZero);
            userToChange.get().setCounterparts(finalSetWithoutZero);
            userDataRepository.save(userToChange.get());
            return "poprawnie dodano Cunterparts";
        } else {
            throw new DivideWisleyExeption(HttpStatus.BAD_REQUEST,"nie ma takiego usera");
        }
    }

    private DeptorOrCreditor reverceDeptorOrCreditor(DeptorOrCreditor deptorOrCreditor) {
        if (deptorOrCreditor.equals(DeptorOrCreditor.CREDITOR)) {
            return DeptorOrCreditor.DEPTOR;
        } else if (deptorOrCreditor.equals(DeptorOrCreditor.DEPTOR)) {
            return DeptorOrCreditor.CREDITOR;
        } else {
            throw new NullPointerException();
        }
    }

}
