package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.Found;
import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.model.other.DeptorOrCreditor;
import com.example.divideWisely.repository.FoundRepository;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import com.example.divideWisely.repository.UsersGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UsersGroupService {
    @Autowired
    FoundRepository foundRepository;
    @Autowired
    UsersGroupRepository usersGroupRepository;
    @Autowired
    CounterpartService counterpartService;
    @Autowired
    LoggedUserRepository loggedUserRepository;
    @Autowired
    UserDataRepository userDataRepository;

    public String createGroup(UsersGroup usersGroup, String token) throws DivideWisleyExeption {
        if (loggedUserRepository.existsByToken(token)) {
            usersGroup.getMembers().add(loggedUserRepository.findByToken(token).get().getUserData());
            UsersGroup usersGroupWithMembers = new UsersGroup(null, usersGroup.getName(), new ArrayList<>());
            Set<UserData> members = new HashSet<>();
            try {
                usersGroup.getMembers()
                        .forEach(member -> members.add(userDataRepository.findById(member.getId()).get()));
            } catch (Exception e) {
                throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"no users found");
            }
            if (members.size() < 2) {
                throw  new DivideWisleyExeption(HttpStatus.BAD_REQUEST,"group must have at least 2 members");
            } else {
                usersGroupWithMembers.setMembers(members);
                usersGroupRepository.save(usersGroupWithMembers);
                return "grupa " + usersGroup.getName() + " utworzona";
            }
        } else {
            throw new DivideWisleyExeption(HttpStatus.UNAUTHORIZED,"you are not logged in");
        }
    }

    public UsersGroup getGroup(long id, String token) throws DivideWisleyExeption {
        Optional<UsersGroup> usersGroup = usersGroupRepository.findById(id);
        if (usersGroup.isPresent()) {
            if (token.equals("password") || (loggedUserRepository.existsByToken(token) && usersGroup.get().getMembers().stream().anyMatch(userData -> userData.getId() == loggedUserRepository.findByToken(token).get().getUserData().getId()))) {
                usersGroup.get().getFounds().forEach(found -> found.getFounder().setCounterparts(null));
                usersGroup.get().getMembers().forEach(userData -> userData.setCounterparts(null));
                return usersGroup.get();
            } else {
                throw new DivideWisleyExeption(HttpStatus.UNAUTHORIZED,"Unauthorized");//lepszy massage
            }
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"not found");//lepszy massage
        }
    }

    public String found(String foundName, Double prize, long groupId, String token) throws DivideWisleyExeption {
        Optional<LoggedUser> founderByToken = loggedUserRepository.findByToken(token);
        if (founderByToken.isPresent()) {
            UserData founder = founderByToken.get().getUserData();
            if (usersGroupRepository.findById(groupId).isPresent()) {
                UsersGroup usersGroup = usersGroupRepository.findById(groupId).get();
                Found found = new Found(founder, prize, foundName);
                usersGroup.getFounds().add(found);
                foundRepository.save(found);
                usersGroupRepository.save(usersGroup);
                Set<Long> membersIds = new HashSet<>();
                usersGroup.getMembers().forEach(member -> membersIds.add(member.getId()));
                if (!membersIds.remove(founder.getId())) {
                    throw new DivideWisleyExeption(HttpStatus.BAD_REQUEST,"user who gives money to a group cannot possibly not belong to it");
                }
                counterpartService.upDateCounterpartForOneSide(membersIds, founder.getId(), found.getPrize() / (membersIds.size() + 1), DeptorOrCreditor.DEPTOR);
                for (Long memberId : membersIds) {
                    counterpartService.upDateCounterpartForOneSide(Collections.singleton(founder.getId()), memberId, found.getPrize() / (membersIds.size() + 1), DeptorOrCreditor.CREDITOR);
                }
                return "poprawnie zafudowano " + found.getFundName();

            } else {
                throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"group not found");
            }
        } else {
            throw new DivideWisleyExeption(HttpStatus.NOT_FOUND,"You are not logged in");
        }
    }

}
