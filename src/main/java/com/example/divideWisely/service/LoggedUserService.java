package com.example.divideWisely.service;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserLoginData;
import com.example.divideWisely.repository.LoggedUserRepository;
import com.example.divideWisely.repository.UserDataRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.beans.Transient;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
public class LoggedUserService {
    @Autowired
    UserDataRepository userRepository;
    @Autowired
    LoggedUserRepository loggedUserRepository;

    @Transactional
    public String loginUser(UserLoginData userLoginData) throws DivideWisleyExeption {
        if (userRepository.existsByName(userLoginData.getName())) {
            if (userRepository.findByName(userLoginData.getName()).get().getPassword().equals(userLoginData.getPassword())) {
                if (loggedUserRepository.existsByUserData_Name(userLoginData.getName())) {
                    loggedUserRepository.deleteByUserData_Name(userLoginData.getName());
                }
                String token = createToken(userLoginData.getName());
                loggedUserRepository.save(new LoggedUser(userRepository.findByName(userLoginData.getName()).get(), token));

                return token;
            } else {
                throw new DivideWisleyExeption(HttpStatus.FORBIDDEN,"złe hasło");
            }

        } else {
            throw  new DivideWisleyExeption(HttpStatus.BAD_REQUEST,"nie ma takiego usera");
        }
    }

    @Transactional
    public String logoutUser(String token) throws DivideWisleyExeption {
        if (loggedUserRepository.existsByToken(token)) {
            loggedUserRepository.deleteByToken(token);
            return "poprawnie wylogowano";
        } else {
            throw new DivideWisleyExeption(HttpStatus.UNAUTHORIZED,"this token does not exist");
        }
    }

    public String createToken(String name) {
        // Łączenie imienia, aktualnego czasu i jakiegoś sekretnego klucza (może być puste)
        String input_data = name + System.currentTimeMillis() + "ntv2y0y947nyr3974nyntcv8732y1c0234095osdjhfjdhfgk23irhuwfaisjfpiawfujqwepr23ru7r23y0n9x43d9";

        try {
            // Utworzenie instancji MessageDigest z algorytmem SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            // Obliczenie skrótu
            byte[] hashBytes = md.digest(input_data.getBytes());

            // Konwersja bajtów na szesnastkowy format
            StringBuilder hexStringBuilder = new StringBuilder();
            for (byte b : hashBytes) {
                hexStringBuilder.append(String.format("%02x", b));
            }

            // Zwrócenie wyniku jako unikalnego tokenu
            return hexStringBuilder.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            // Możesz obsłużyć wyjątek związanego z nieznalezieniem algorytmu
            return null;
        }
    }
}
