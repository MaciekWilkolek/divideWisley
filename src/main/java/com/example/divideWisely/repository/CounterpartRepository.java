package com.example.divideWisely.repository;
import com.example.divideWisely.model.Counterpart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterpartRepository extends JpaRepository<Counterpart, Long> {
}
