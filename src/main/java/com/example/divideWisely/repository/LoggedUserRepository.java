package com.example.divideWisely.repository;


import com.example.divideWisely.model.LoggedUser;
import com.example.divideWisely.model.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoggedUserRepository extends JpaRepository<LoggedUser, Long> {
    boolean existsByUserData_Name(String name);

    void deleteByToken(String token);

    boolean existsByToken(String token);

    @Query("SELECT lu FROM LoggedUser lu WHERE lu.token = :token")
    Optional<LoggedUser> findByToken(@Param("token") String token);

    @Modifying
    @Query("DELETE FROM LoggedUser lu WHERE lu.userData.name = :name")
    void deleteByUserData_Name(@Param("name") String name);
}
