package com.example.divideWisely.repository;

import com.example.divideWisely.model.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {
    boolean existsByName(String name);
    Optional<UserData> findByName(String name);
}