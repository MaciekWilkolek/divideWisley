package com.example.divideWisely.repository;

import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersGroupRepository extends JpaRepository<UsersGroup, Long> {
    List<UsersGroup> findAllByMembersContaining(UserData userData);
}
