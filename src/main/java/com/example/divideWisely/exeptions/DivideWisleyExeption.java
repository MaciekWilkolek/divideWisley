package com.example.divideWisely.exeptions;

import org.springframework.http.HttpStatus;

public class DivideWisleyExeption extends Exception {
    private final HttpStatus httpStatus;

    public DivideWisleyExeption(HttpStatus httpStatus,String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
