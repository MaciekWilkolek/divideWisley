package com.example.divideWisely;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class DivideWiselyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DivideWiselyApplication.class, args);
	}

}
