package com.example.divideWisely;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DivideWisleyExeption.class)
    public ResponseEntity<String> handleDivideWisleyException(DivideWisleyExeption ex) {
        return ResponseEntity.status(ex.getHttpStatus()).body(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exeption() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("nie wiemy co nie działa");
    }
}
