package com.example.divideWisely.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Found {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private UserData founder;
    private double prize;
    private String fundName;

    public Found(UserData founder, double prize, String fundName) {
        this.founder = founder;
        this.prize = prize;
        this.fundName = fundName;
    }
}
