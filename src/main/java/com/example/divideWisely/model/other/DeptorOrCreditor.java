package com.example.divideWisely.model.other;

import jakarta.persistence.Entity;

public enum DeptorOrCreditor {
    DEPTOR,
    CREDITOR,
}
