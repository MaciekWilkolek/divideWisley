package com.example.divideWisely.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class UsersGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToMany
    private Set<UserData> members;
    private String name;
    @OneToMany
    private List<Found> founds;

    public UsersGroup(Set<UserData> members, String name, ArrayList<Found> founds) {
        this.members = members;
        this.name = name;
        this.founds = founds;
    }


}
