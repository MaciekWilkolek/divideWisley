package com.example.divideWisely.model;

import com.example.divideWisely.model.other.DeptorOrCreditor;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Counterpart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    @ManyToOne
    UserData userData;
    private DeptorOrCreditor deptorOrCreditor;
    private double prize;

    public Counterpart(UserData userData, DeptorOrCreditor deptorOrCreditor, double prize) {
        this.userData = userData;
        this.deptorOrCreditor = deptorOrCreditor;
        this.prize = prize;
    }
}
