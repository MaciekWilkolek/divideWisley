package com.example.divideWisely.controler;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.UserData;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.service.UserDataService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Controller
public class UserControler {
    @Autowired
    UserDataService userService;

    @PostMapping("/createUser")
    ResponseEntity<UserData> createUser(@RequestBody UserData userData) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(userService.addUserData(userData));
    }

    @GetMapping("/getUserDataForAdmin/{userId}/")
    ResponseEntity<UserData> getUserDataForAdmin(@PathVariable long userId, @RequestHeader String password) throws DivideWisleyExeption {
        if (password.equals("password")) {
            return ResponseEntity.status(HttpStatus.OK).body(userService.getUserForAdmin(userId));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @GetMapping("/getUserData")
    ResponseEntity<UserData> getUserData(@RequestHeader String token) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserData(token));
    }

    @GetMapping("/getUserIdByName")
    ResponseEntity<Long> getUserIdByName(@RequestParam String name) throws DivideWisleyExeption {
        return  ResponseEntity.status(HttpStatus.OK).body(userService.getUserIdByName(name));
    }
    @GetMapping("/getUserGroups")
    ResponseEntity<Set<UsersGroup>> getUserGroups(@RequestHeader String token) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserGroups(token));
    }
}
