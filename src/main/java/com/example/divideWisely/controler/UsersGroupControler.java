package com.example.divideWisely.controler;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.Found;
import com.example.divideWisely.model.UsersGroup;
import com.example.divideWisely.service.UsersGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;

@Controller
public class UsersGroupControler {
    @Autowired
    private UsersGroupService usersGroupService;

    @PostMapping("/createGroup")
    ResponseEntity<String> createGroup(@RequestBody UsersGroup usersGroup, @RequestHeader String token) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(usersGroupService.createGroup(usersGroup,token));
    }

    @GetMapping("/getGroup/{id}")
    ResponseEntity<UsersGroup> getGroup(@PathVariable long id, @RequestHeader String token) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(usersGroupService.getGroup(id, token));
    }

    @PostMapping("/found")
    ResponseEntity<String> found(@RequestHeader String foundName, @RequestHeader Double prize, @RequestHeader long groupId, @RequestHeader String token) throws DivideWisleyExeption {
        return  ResponseEntity.status(HttpStatus.OK).body(usersGroupService.found(foundName, prize, groupId, token));
    }
}
