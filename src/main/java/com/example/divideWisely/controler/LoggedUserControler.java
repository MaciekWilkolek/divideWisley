package com.example.divideWisely.controler;

import com.example.divideWisely.exeptions.DivideWisleyExeption;
import com.example.divideWisely.model.UserLoginData;
import com.example.divideWisely.service.LoggedUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@Controller
public class LoggedUserControler {
    @Autowired
    LoggedUserService loggedUserService;

    @PostMapping("/loginUser")
    ResponseEntity<String> loginUser(@RequestBody UserLoginData userLoginData) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(loggedUserService.loginUser(userLoginData));
    }

    @GetMapping("/logoutUser")
    ResponseEntity<String> logoutUser(@RequestHeader String token) throws DivideWisleyExeption {
        return ResponseEntity.status(HttpStatus.OK).body(loggedUserService.logoutUser(token));
    }
}
